import http from '../common/vmeitime-http/interface'

export const api_login = (data) => {
    return http.request({
        url: 'common/login',
		method:'POST',
		dataType: 'text',
        data,
    })
}
export const api_login_reg = (data)=>{
	return http.request({
		url:'common/register',
		method:'POST',
		data:data
	})
}